const MagmaScript = require('../main')
const path = require('path')
const fs = require('fs')

new MagmaScript({
    input: path.join(__dirname, 'src/std.mg'),
    output: path.join(__dirname, 'src/standard.js'),
    std: false,
    bundle: true,
    strict: false,
    compilerComments: false
})

let footer = '\n\n/* Your Code Starts here: */\n\n'

let additional = fs.readFileSync(path.join(__dirname, 'src/standard.js'), 'utf-8')

let base = fs.readFileSync(path.join(__dirname, 'standard.js'), 'utf-8')
fs.writeFileSync(path.join(__dirname, '../standard.js'), base + additional + footer)
