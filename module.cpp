#include <node_api.h>


napi_value New(napi_env env, napi_callback_info info) {
    
    size_t argc = 1;
    napi_value args[argc];

    napi_get_cb_info(env, info, &argc, args, NULL, NULL);  
    napi_value ret;
    napi_create_array(env, &ret);
    uint32_t i, length;
    napi_get_array_length(env, args[0], &length);

    for (i = 0; i < length; i++) {
        napi_value e;
        napi_get_element(env, args[0], i, &e);
        napi_set_element(env, ret, i, e);
    }
    return ret;
}

napi_value Init(napi_env env, napi_value exports) {
    napi_status status;
    napi_value fn;

    napi_create_function(env, NULL, 0, New, NULL, &fn);
    napi_set_named_property(env, exports, "New", fn);

    return exports;
}

NAPI_MODULE(NODE_GYP_MODULE_NAME, Init)