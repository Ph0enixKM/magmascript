const chalk = require('chalk')
const { random } = require('../addons/helpers')

/**
 * This module provides erroring system for Magma compiler
 */

const errorTypes = {
    WARNING: 0,
    ERROR: 1,
    ERROR_SYNTAX: 2    
}

module.exports = {
    error,
    errorTypes
}

const messageType = [
    'Warning!\n',
    'Hmm... I suddenly came across an error!\n',
    'Oopsie! I guess I found a typo in your code!\n'
]

const friendly = [
    'man', 'buddy', 'dude', 'bro', 'mate', 'partner'
]

const look = [
    'Let\'s have a closer look', 
    'Check this line', 
    'It\'s somewhere right here', 
    'I think it\'s this line',
    'Right here, look'
]

/**
 * 
 * @param {Number} obj.type - Error Type
 * @param {String} obj.text - Error message
 * @param {Array} obj.data - File data lines
 * @param {Number} obj.index - Current line
 * @param {String} obj.filename - Filename
 */

function error(obj) {
    let message = ''
    let i = obj.index
    let data = obj.data

    // Set appropriate color for the error
    let color = chalk.red.bind(chalk)
    if (!obj.type) color = chalk.yellow.bind(chalk)

    message += 'In: ' + obj.filename + '\n\n'
    message += messageType[obj.type]
    message += obj.text  + ', ' + friendly[Math.floor(random(friendly.length))] + '...\n'
    message += look[Math.floor(random(look.length))] + ':\n\n'

    console.error(color(message))

    if ((i - 1) >= 0) console.log(i + ' |', data[i - 1])
    console.log(i + 1 + ' | ' + chalk.cyan(data[i]))
    if ((i + 1) < data.length) console.log(i + 2 + ' |', data[i + 1])
    
    if (obj.type) process.exit(obj.type)
}