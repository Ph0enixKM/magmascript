#include <napi.h>
#include <iostream>
#include <vector>
#include <map>
#include <chrono>
#include <ctime>

using std::string;
template <class T> void log(T val) { std::cout << val << '\n';}

template <class T>
class Array {
    std::vector<T> arr;
    public:
    int length = 0;

    Array(Napi::Array given, int cursor) {
        for (int i = 0; i < given.Length(); i++) {
            arr.push_back(given.Get((uint32_t)i).ToNumber());
        }
        length = arr.size();
    }

    T& operator[](int index) {
        return arr[index];
        length = arr.size();
    }
};

class Time_Type {
    std::map<string, std::chrono::steady_clock::time_point> timers;
    std::chrono::steady_clock::time_point def = std::chrono::steady_clock::now();

    
    public:
    Time_Type() {}

    void start(string id) {
        timers[id] = std::chrono::steady_clock::now();
    }

    void start() {
        def = std::chrono::steady_clock::now();
    }

    float stop(string id) {
        auto end = std::chrono::steady_clock::now();
        std::chrono::steady_clock::duration remained = end - timers[id];
        double nseconds = double(remained.count()) * std::chrono::steady_clock::period::num / std::chrono::steady_clock::period::den;
        
        timers.erase(id);
        return (float)nseconds * 1000;
    }

    float stop() {
        auto end = std::chrono::steady_clock::now();
        std::chrono::steady_clock::duration remained = end - def;
        double nseconds = double(remained.count()) * std::chrono::steady_clock::period::num / std::chrono::steady_clock::period::den;
        return (float)nseconds * 1000;
    }

    float now() {
        time_t timer;
        return (float)time(&timer);

    }

};
Time_Type Time;

