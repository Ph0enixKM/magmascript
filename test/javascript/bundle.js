const $root = ''
/**
 * Standard Library For MagmaScript Language
 * Copyright 2019 - Phoenix Arts (Paweł Karaś)
 */

function log(...content) {
    console.log(...content)
}

function random(min = 0, max = 1) {
    return (Math.random() * max) + min
}

function sqrt(number) {
    return Math.sqrt(number)
}

let get = () => log('Magma Warning: \'get\' function is not suppoerted in this enviroment');

// If involves Node.js compiler
if (typeof require == 'function') {
    get = (() => {
        const readline = require($root + 'readline-sync')

        function get(question = '', type, obj) {

            if (typeof type === 'undefined') {
                return readline.question(question)
            }

            if (type === 'password') {
                return readline.question(question, {
                    hideEchoBack: true
                })
            } else if (type === 'yes-no') {
                return readline.keyInYN(question)
            } else if (type === 'select') {
                return readline.keyInSelect(obj, question)
            } else if (type === 'loop') {
                if (question.length) console.log(question)
                return readline.promptCLLoop(obj)
            } else if (type === 'float') {
                return readline.questionFloat(question)
            } else if (type === 'int') {
                return readline.questionInt(question)
            } else if (type === 'email') {
                return readline.questionEMail(question)
            } else if (type === 'path') {
                return readline.questionPath(question)
            }
        }

        return get
    })()

}

Array.prototype.last = function() {
    return this[this.length - 1]
}

String.prototype.lower = function() {
    return this.toLowerCase()
}

String.prototype.upper = function() {
    return this.toUpperCase()
}

String.prototype.firstLower = function() {
    return this[0].toLowerCase() + this.substring(1)
}

String.prototype.firstUpper = function() {
    return this[0].toUpperCase() + this.substring(1)
}

// If involves HTML
if (typeof(document) != 'undefined') {
    function element(query) {
        return document.querySelector(query)
    }

    function elements(query) {
        return new Array(...document.querySelectorAll(query))
    }

    HTMLElement.prototype.css = function(key, val) {
        let newKey = ''

        for (let i = 0; i < key.length; i++) {
            if (key[i] == '-')
                newKey += key[++i].toUpperCase()
            else
                newKey += key[i]
        }

        this.style[newKey] = val
    }

    HTMLElement.prototype.magmaEvents = {}

    HTMLElement.prototype.on = function(event, callback, options = false) {
        let stateList = this.magmaEvents[event]

        this.magmaEvents[event] = (stateList == null) ? [] : stateList
        this.magmaEvents[event].push([callback, options])

        this.addEventListener(event, callback, options)

        return (this.magmaEvents[event].length - 1)
    }

    HTMLElement.prototype.detach = function(event, id = null) {
        if (isNaN(id)) {
            for (let index = 0; index < this.magmaEvents[event].length; index++) {
                this.removeEventListener(event, ...this.magmaEvents[event][index])
            }
        } else {
            this.removeEventListener(event, ...this.magmaEvents[event][id])
        }
    }

    HTMLElement.prototype.parent = function(up = 1) {
        let el = this

        for (; up > 0; up--) {
            el = el.parentElement
        }

        return el
    }
}

let Time = null
if (typeof require != 'undefined') {
    const {
        performance
    } = require('perf_hooks')

    class Time_Type {
        constructor() {
            this.timers = {}
            this.def = performance.now()
        }

        start(label) {
            if (label) {
                this.timers[label] = performance.now()
            } else {
                this.def = performance.now()
            }
        }

        stop(label) {
            let end = performance.now()

            if (label) {
                return end - this.timers[label]
            } else {
                return end - this.def
            }
        }

        now() {
            return new Date().getTime()
        }
    }

    Time = new Time_Type()
}

/**
 * STL Code:
 */
const File = (() => {
    const fs = require(`` + `fs`)

    class File {
        constructor(path) {
            this.path = path
        }

        read(encoding = `utf-8`) {
            return fs.readFileSync(this.path, encoding)
        }

    }

    return File
})()

/* Your Code Starts here: */

'use strict';

// @magma C:\Users\Phoenix\Desktop\Projects\magmascript\test\magma\target.mg:1
let ignore = new File(`.gitignore`).read()
// @magma C:\Users\Phoenix\Desktop\Projects\magmascript\test\magma\target.mg:2
log(`Remember, NPM to ignore: \n ${ignore}`)