# Magma Script Language
<div align="center">
    <img width="150" src="https://bitbucket.org/Ph0enixKM/vsc-magma-language/raw/86414b15804dfe1f86db8dba057e67d3a06ad473/images/icon.png">
</div>

## Magma Translator to JavaScript
### How to use it?

```js
const MagmaScript = require('magma-script')

new MagmaScript({
    input: __dirname + '/target.mg',
    output: __dirname + '/bundle.js',
    std: true, // Defualt (true)
    strict: true, // Default (true)
    bundle: true, // Default (true)
    global: false, // Default (false)
    root: '/usr/bin/node/', // Default ('/usr/bin/node/')
    beautify: true // Default (true)
})
```

## Flags:
- input - Specify full path to your MagmaScript file
- output - Specify full path if you want to save file (optional)
- std - If you want to include Magma's Standard Library to this file (optional)
- strict - If you want to append 'use strict' to the file (optional)
- bundle - If you want to bundle all non-node files included in this script and it's descendants (optional)
- global - If you want to insert ```#!/usr/bin/env node``` header before
- root - If you want to use different Node.js compiler
- beautify - If you want to beautify the output code


## What if I don't want to write to a file?

```js
let data = new MagmaScript({
    input: __dirname + '/target.mg'
}).result

// 'data' now contains the translated MagmaScript file
```

## You can also compile the entire directory using this syntax
```js
new MagmaScript.Directory({
    input: __dirname + '/src',
    output: __dirname + '/out',
    std: 'main.mg', // Default (null)
    strict: true // Default (true)
})

// Every .mg file will be translated and written to specified directory
```

## This package comes with Magma-to-JS compilator
All you need to do is to install this package globally:
```bash
$ npm install magma-script -g
```

Here are some usage examples:

```bash
# Possible commands:
# mg <input file> <output file>
# mg help


$ magma-script input.mg output.js

# Or:

$ mg input.mg output.js
```

## Flags:
- input - Specify full path to your MagmaScript files folder
- output - Specify full path to directory where you want to save translated files
- std - File name of main Magma source file to which Standard Library will be attached (optional)
- strict - If you want to append 'use strict' to all the files (optional)

However if you want to bundle all the files into one standalone, please use the base MagmaScript class compiler with flag 'bundle' turned as true

## Example MagmaScript Code

```js
import { remote } from 'electron'

let arr = [1, 3, 4]
let i = 0

export i

// For loop
loop item in arr {
    console.log(item)
}

// For loop (index, value)
loop index, value in arr {
    // You can interpolate variables inside of a regular string
    console.log('index ${index}, value ${value}')
}

// While loop
loop (arr.lenght > i) {
    i++
}

// While True loop
loop {
    console.log('again and...')
}



// Object Structure
tree myObject {
    age = 18
    name = 'John'
}

// Function (type is optional)
fun void add(a, b) {
    return a + b
}

// Class (setup function is constructor)
> Hero {
    setup(obj) {
        this.name = obj.name
    }
}

// Class with inheritance
> Mummy : Granny {
    setup(obj) {
        super(obj)
    }
}
```

# Standard Library Functions
```js

// // In Browser Only:
let el = element('#id') // document.querySelector('#id')
let els = elements('.class') // document.querySelectorAll('.class')
el.css('border-radius', '10px') // el.style.borderRadius = '10px'
el.parent(3) // Returns parent 3 elements up in hierarchy

// Event Listeners
el.on('click', () => log('Hello!')) // Add Event Listener

// Get id of the event listener
let id = el.on('click', () => {
    log('Clicked!')
})

el.detach('click', id) // Remove 'click' event listener with the specific ID
el.detach('click') // Removes all 'click' event listeners



// // Everywhere:
log('Hello World') // console.log('Hello World')
random(5, 10) // (Math.random() * 10) + 5
sqrt(64) // Math.sqrt(64)
log('HELLO'.lower(), 'world'.upper()) // logs: hello WORLD
log('john'.firstUpper()) // logs: John
log('Fun'.firstLower()) // logs: fun

let arr = [1, 2, 3]

arr.last() // arr[arr.length - 1]

// The standard library is going to be expanded soon
```


## Note:
Writing vanilla JavaScript in MagmaScript is valid.

# Thank you for using this package.