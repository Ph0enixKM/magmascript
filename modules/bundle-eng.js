/**
 * Bundling Engine made for MagmaScript Compiler
 * Copyright 2019 - Phoenix Arts (Paweł Karaś)
 */

const path = require('path')
const fs = require('fs')
const { clearSyntax, insertToArr, syntaxFilter, getFirstValue } = require('../addons/helpers')
const { error, errorTypes } = require('../addons/errors')
const { ERROR } = errorTypes

module.exports = (raw, fullUrl, compilerComments = true) => {    
    
    let data = syntaxFilter(raw, false, false)

    // Data with no strings
    let keywords = syntaxFilter(raw, true, false)
    
    // Current path to folder (__dirname immitation)
    // let url = fullUrl.replace(/\/[^\/]+\.mg/, '/') // TEMP
    let url = path.dirname(path.resolve(fullUrl))

    // Relative Paths Map
    let paths = new Array(data.length).fill(url)
    
    // All RegExp patterns
    let reg = {
        simpleImport : /\bimport\b/,
        complexImport : /import\s+(.*)\s+from\s+'((\.\.\/|\.\/|\/).*)';?/,
        export : /\bexport\b/g
    }

    // Map variable setup
    let map = []
    let keyMap = []

    // Map lines for validation
    if (compilerComments) {
        for (const [i, line] of data.entries()) {
            map.push(`// @magma ${path.resolve(fullUrl)}:${i+1}`)
            keyMap.push('// @magma')
    
            if (reg.export.test(keywords[i])) {
                reg.export.lastIndex = 0
                
                map.push(line.replace(reg.export, `log('This module would export evaluated: \'`) + `, '\' if it was imported by other file')`)
                keyMap.push(keywords[i].replace(reg.export, `return`))
            }
    
            else {
                map.push(line)
                keyMap.push(keywords[i])
            }
        }
    }


    // Reassign maps to main modules
    if (compilerComments) {
        data = map
        keywords = keyMap
    }
    
    
    
    // Main translation loop
    for (let index = 0; index < data.length; index++) {
        importExport(data, keywords, index)
        // throwMessaging(data, keywords, index)
    }    
        

    return data




    function importExport(data, keywords, index) {
        let line = data[index]

        // Enter if it's import
        if (reg.simpleImport.test(keywords[index])) {
            
            
            if (reg.complexImport.test(line)) {                
                // reg.complexImport.lastIndex = 0

                // Get url and name of the module
                let res = reg.complexImport.exec(line)
                let passUrl = path.join(paths[index], res[2])
                let url = passUrl.replace(/\/[^\/]+\.mg/, '/') // TEMP
                // let url = path.dirname(passUrl)
                let importName = res[1]
                
                // Ensure that the file exists
                if (!fs.existsSync(passUrl)) error({
                    type: ERROR,
                    text: 'Couldn\'t load:  ' + url,
                    data,
                    index,
                    filename: path.resolve(fullUrl)
                })
                
                
                // Ensure that it's not a directory
                if (fs.lstatSync(passUrl).isDirectory()) error({
                    type: ERROR,
                    text: 'Specify file path. Couldn\'t load directory:  ' + url,
                    data,
                    index,
                    filename: path.resolve(fullUrl)
                })
                
                
                // Import module to memory
                let rawData = fs.readFileSync(passUrl, 'utf-8').split(/\r?\n/)
                let rawModule = syntaxFilter(rawData, false, false)
                
                
                let bakedModule = syntaxFilter(rawModule, true, false)
                let mappedModule = []
                let mappedBakedModule = []
                
                
                // Rename exports to the name of the module
                for (const [i, line] of bakedModule.entries()) {
                    
                    if (compilerComments) {
                        mappedModule.push(`// @magma ${passUrl}:${i+1}`)
                        mappedBakedModule.push('// @magma')
                    }
                    
                    if (reg.export.test(line)) {
                        reg.export.lastIndex = 0
                        
                        mappedModule.push(rawModule[i].replace(reg.export, `return`))
                        mappedBakedModule.push(bakedModule[i].replace(reg.export, `return`))
                    }
                    
                    else {
                        mappedModule.push(rawModule[i])
                        mappedBakedModule.push(bakedModule[i])
                    }
                }

                // Reassign maps to modules                
                rawModule = mappedModule
                bakedModule = mappedBakedModule

                // Append neeed behavior to modules
                rawModule.unshift(`const ${importName} = (() => {`)
                bakedModule.unshift(`const ${importName} = (() => {`)
                rawModule.push(`})()`)
                bakedModule.push(`})()`)
                
                // Insert imported data to the entire file
                data = insertToArr(data, index, rawModule)
                keywords = insertToArr(keywords, index, bakedModule)
                
                // Insert new context to Path's map
                let map = new Array(rawModule.length).fill(url)
                paths = insertToArr(paths, index, map)                
                
                index -= 4
            }
        }
    }
    
}



function throwMessaging (data, keywords, i) {
    let regSimple = /throw/
    let regComplex = /throw(.*)$/

    if (regSimple.test(keywords[i])) {
        if (regComplex.test(keywords[i])) {
            let res = regComplex.exec(data[i])
            let compiled = res[1]
            // TODO
            

        }
    }
}
