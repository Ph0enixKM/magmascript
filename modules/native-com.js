/**
 * Main Native function compiler
 * Copyright 2019 Phoenix Arts (Paweł Karaś)
 */

const fs = require('fs')
const path = require('path')
const { syntaxFilter, replaceInKeywords } = require('../addons/helpers')


module.exports = native => {
    let { code } = native    

    let data = syntaxFilter(code, false, true, true)
    let keywords = syntaxFilter(code, true, true, true)    

    for (let i = 0; i < data.length; i++) {

        getVar(data, keywords, i)
        getReturn(data, keywords, i)
        getStrings(data, keywords, i)
        getLoop(data, keywords, i)
        
        getSemicolon(data, keywords, i)

    }

    // Generate C++ File
    function generateFile () {
        let header = `Napi::Value ${native.name} (const Napi::CallbackInfo& info) {\n`
        header += `Napi::Env env = info.Env();\n`

        for (let i = 0; i < native.params.length; i++) {
            let param = native.params[i]
            let add = ''

            if (param[1] == 'int')
                add = `int ${param[0]} = info[0].ToNumber();`


            else if (param[1] == 'float')
                add = `float ${param[0]} = info[0].ToNumber();`


            else if (param[1] == 'bool')
                add = `bool ${param[0]} = info[0].ToBoolean();`


            else if (param[1] == 'string')
                add = `string ${param[0]} = info[0].ToString();`

            else if(param[1][0] == '[' && param[1].last() == ']')
                add = `Array<int> ${param[0]} (info[0].As<Napi::Array>(), (int)0);`


            header += `\n${add}\n`

        }
        header += `\n${data.join('\n')}\n}\n\n`

        return header
    }

    return generateFile()
}

/**
 * Translate variable creation
 */

function getVar(data, keywords, i) {
    let simpleReg = /(let|const)\s+(\S+)(\s+[^\s\=]+)?/
    let noValReg = /(let|const)\s+(\S+)(\s+\S+)\s*$/

    if (simpleReg.test(keywords[i])) {
        
        let res = simpleReg.exec(data[i])
        let name = res[3] ? res[3] : res[2]
        name = name.trim()
        let type = res[2] == name ? 'auto' : res[2]
        
        
        if (noValReg.test(data[i])) {
            let add = ''
            
            if (type == 'int') add = ' = 0'
            else if (type == 'float') add = ' = 0.0f'
            else if (type == 'bool') add = ' = false'
            
            data[i] += add
        }
        
        let tokens = data[i].trim().split(' ')

        // console.log(type, name);
        if (type == 'auto') {
            tokens.splice(1, 0, 'auto')
            
        }
        
        if (tokens[0] == 'let') data[i] = tokens.slice(1).join(' ')
    }
}


/**
 * Convert C/C++ into Value
 */

function getReturn(data, keywords, i) {
    let simpleReg = /\breturn\b/
    let complexReg = /\breturn\b(.*)$/
    if (simpleReg.test(keywords[i])) {        
        let res = complexReg.exec(data[i])
        let compiled = `return Napi::Value::From(env, (${res[1]}))`
        data[i] = compiled
    }
}


/**
 * Transform Strings into C/C++ std::string
 */

function getStrings(data, keywords, i) {
    let res = ''
    let str = false

    for (let j = 0; j < data[i].length; j++) {
        let sym = data[i][j]
        let prev = (j) ? data[i][j - 1] : data[i][j]


        if (sym == '\"' && prev != '\\') {
            if (!str) res += 'string("'
            else res += '")'

            str = !str
            continue
        }

        res += sym
    }

    data[i] = res
}

/**
 * Loop Transforming into C/C++ loops
 */

function getLoop (data, keywords, i) {
    let simpleReg = /^\s*loop/
    let arrayReg = /^\s*loop\s+([^,\s]+)\s*in\s+(.*)\s*(?=\{)/
    let whileReg = /^\s*loop\s+\((.*)\)/
    let rangeReg = /^\s*loop\s+([^,\s]+)\s+to\s+(.*)\s*(?=\{)/
    let rangeFromReg = /^\s*loop\s+([^,\s]+)\s+from\s+(.*)\s+to\s+(.*)\s*(?=\{)/
    let indexReg = /^\s*loop\s+(\S+)\s*,\s*(\S+)\s+in\s+(.*)\s*(?=\{)/

    if (whileReg.test(keywords[i])) { //
        let res = whileReg.exec(data[i])
        let compiled = `while (${res[1]})`
        data[i] = data[i].replace(whileReg, compiled)
    }

    // TODO: Finish it after you add Array Support
    // else if (indexReg.test(keywords[i])) {
    //     let res = indexReg.exec(data[i])
    //     res[3] = res[3].trim()
    //     let compiled = `int ${res[1]} = 0; \n auto ${res[2]} = (${res[3]}.size()) ? ${res[3]} : NULL; for ()`

    //     let compiled = `for (let [${res[1]}, ${res[2]}] of (typeof(${res[3]}) == 'string') 
    // ? ${res[3]}.split('').entries() 
    // : (Array.isArray(${res[3]})
    // ? ${res[3]}.entries()
    // : Object.entries(${res[3]})))`
    //     data[i] = data[i].replace(indexReg, compiled)
    // }
    
    
    else if (rangeFromReg.test(keywords[i])) {
        let res = rangeFromReg.exec(data[i])
        let compiled = `for (int ${res[1]} = ${res[2]}; ${res[1]} < ${res[3]}; ${res[1]}++ )`
        data[i] = data[i].replace(rangeFromReg, compiled)
    }

    else if (rangeReg.test(keywords[i])) {
        let res = rangeReg.exec(data[i])
        let compiled = `for (int ${res[1]} = 0; ${res[1]} < ${res[2]}; ${res[1]}++ )`
        data[i] = data[i].replace(rangeReg, compiled)
    }

    else if (arrayReg.test(keywords[i])) {
        let res = arrayReg.exec(data[i])
        res[2] = res[2].trim()
        let compiled = `for (auto ${res[1]} : ${res[2]})`
        data[i] = data[i].replace(arrayReg, compiled)
    }

    else if (simpleReg.test(keywords[i])) { //
        data[i] = data[i].replace(simpleReg, 'while(true)')   
    }
}


/**
 * Assemble semicolon in the end
 */

 function getSemicolon(data, keywords, i) {
    let lastChar = data[i].trim().last()
    if (lastChar != ';' && lastChar != '{') {
        data[i] += ';'
    }
 }

