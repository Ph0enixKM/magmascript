/**
 * Magma Script - Magma language syntax for JavaScript
 * Copyright 2019 Phoenix Arts (Paweł Karaś)
 */
const fs = require('fs')
const path = require('path')

// Use required polyfills
const { polyFills } = require('./addons/helpers')
const { error, errorTypes } = require('./addons/errors')

polyFills()

// Available Engines or Compilers

const modules = {
    regCom: require('./modules/reg-com'),
    bundleEng: require('./modules/bundle-eng'),
    beautifyEng: require('beautify'),
    nativeCom: require('./modules/native-com'),
}


/**
 * Main Class
 */

class MagmaScript {
    constructor(options) {

        // Stage 1 - Read file
        this.content = fs.readFileSync(options.input, {
            encoding: 'utf-8'
        })

        // Stage 2 - Split file content to array
        this.data = this.content.split(/\r?\n/)

        // Stage 3 - Get all the flags
        this.input = options.input || null
        this.output = options.output || false
        this.std = (options.std == null) ? true : options.std
        this.strict = (options.strict == null) ? true : options.strict
        this.bundle = (options.bundle == null) ? true : options.bundle
        this.beautify = (options.beautify == null) ? true : options.beautify
        this.global = (options.global == null) ? false : options.global
        this.root = (options.root == null) ? '' : options.root
        this.compilerComments = (options.compilerComments == null) ? true : options.compilerComments
        // this.buildPath = path.join(this.root, 'build')
        // this.nativePath = path.join(this.root, 'build/Release/obj.target/native.node')
        
        this.isGUI = false
        this.isNative = false
        this.natives = []

        
        
        
        // Stage 4 - Move on to Translation Process
        this.bundleFiles(this.bundle)
        this.translateRegExp()
        // console.log(this.data);

        this.appendStrict(this.strict)
        this.appendStd(this.std)
        this.beautifyData(this.beautify)
        this.makeGlobal(this.global)
        this.writeFile()
        
    }

    /**
     * Generate global package header before the content
     * @param {Boolean} bool 
     */

    makeGlobal(bool) {
        if (bool) {
            this.resultData = '#!/usr/bin/env node\n\n' + this.resultData
        }
    }


    /**
     * Beautify Generated Content
     * @param {Boolean} bool 
     */

    beautifyData(bool) {
        if (bool) {
            this.resultData = modules.beautifyEng(this.resultData, {
                format: 'js'
            })
    
            let letReg = /^\s*(?!(function|class|\S+\s*\(.*\)|=>))/
            let newResultData = []
            let newLine = 0
    
            // Get rid of unnecessary new lines
            for (const line of this.resultData.split(/\r?\n/)) {
                let br = (line.trim().length == 0)
                
                // Restrict to 3 enters
                if (newLine > 2 && br) continue
                if (br) {
                    newLine++
                    newResultData.push(line)
                }
    
                else {
                    // If it's not special keyword, one enter should be ok
                    if (letReg.test(line) && newLine > 1)
                        for (let index = 0; index < newLine - 1; index++)
                            newResultData.pop()
                    newLine = 0
                    newResultData.push(line)
                }
            }
            this.resultData = newResultData.join('\n')
        }
    }

    
    /**
     * Run Bundling Engine if ordered
     * @param {Boolean} bool 
     */

    bundleFiles(bool) {
        if (bool) {
            this.data = modules.bundleEng(this.data, this.input, this.compilerComments)
        }
    }


    /**
     * Use Regexp Based Compiler for rapid compilation
     */

    translateRegExp() {
        let compiled = modules.regCom(this.data, this.root, path.dirname(path.resolve(this.input)))
        let res = compiled.data.join('\n')
        this.isGUI = compiled.isGUI
        this.isNative = compiled.isNative
        this.natives = compiled.natives
        this.resultData = res
    }


    /**
     * Append Use Strict if ordered
     * @param {Boolean} bool 
     */

    appendStrict(bool) {
        if (bool) {
            let strict = `'use strict'; \n\n`
            this.resultData = strict + this.resultData
        }
    }


    /**
     * Append Standard Library if ordered
     * @param {Boolean} bool 
     */

    appendStd(bool) {
        if (bool) {
            let $root = `const $root = '${this.root}'`
            let std = fs.readFileSync(__dirname + '/standard.js', 'utf-8')
            this.resultData = $root + std + this.resultData
        }
    }

    /**
     * Write File if ordered
     */

    writeFile() {        
        if (this.output)
            fs.writeFileSync(this.output, this.resultData)
    }


    /**
     * Getter function for getting the compiled code
     */

    get result() {
        return this.resultData
    }

    /**
     * Output Error on need
     */

    throwError(obj) {
        error({
            type: errorTypes.ERROR,
            text: obj.text,
            data: obj.data,
            index: obj.index,
            filename: obj.filename
        })
    }

}


/**
 * For Native Code Compilation
 */

MagmaScript.Native = class {
    constructor(natives) {
        this.natives = natives
        this.compiledPieces = []
        this.src = ''
        this.exportObject = ''

        for (let native of natives) {
            this.compiledPieces.push(this.compile(native))

            this.exportObject += `\nexports.Set(Napi::String::New(env, "${native.name}"), `
            this.exportObject += `Napi::Function::New(env, ${native.name}));\n`
        }

        this.header()
        this.body()
        this.footer()
    }

    /**
     * Compile native function code
     * @param {Object} piece - Object representing native function
     */

    compile (piece) {
        return modules.nativeCom(piece)
    }

    /**
     * Append needed content before
     */

    header() {
        let lib = fs.readFileSync(__dirname + '/addons/lib.cpp', 'utf-8')
        this.src += lib
    }

    /**
     * Append body content
     */

    body () {
        for (const piece of this.compiledPieces) {
            this.src += piece
        }
    }

    /**
     * Append needed content after
     */

    footer () {
        this.src += `Napi::Object Import(Napi::Env env, Napi::Object exports) {`
        this.src += this.exportObject
        this.src += 'return exports;\n}\n\n'
        this.src += 'NODE_API_MODULE(native, Import)'
    }
}
 


/**
 * For Directory compilation
 */

MagmaScript.Directory = class {
    constructor (options) {
        
        // Stage 1 - Get all flags
        this.input = options.input || null
        this.output = options.output || null
        this.std = options.std || null
        this.strict = (options.strict == null) ? false : options.strict
        this.bundle = options.bundle || null

        // Stage 2 - optimize paths and flags
        if (! /\/$/.test(this.input))
            this.input += '/'

        if (! /\/$/.test(this.output))
            this.output += '/'

        if (! /\.mg$/.test(this.std) && this.std)
            this.std += '.mg'

    
        // Stage 3 - Compilation Process
        this.compileFiles()
    }


    /**
     * Compile Multiple Files
     */

    compileFiles() {
        if (this.input && this.output) {
            let dir = this.input
            let out = this.output
    
            let files = fs.readdirSync(dir)
            for (const file of files) {
    
                // Compile only Magma files
                if (/\.mg$/.test(file)) {
                    let url = dir + file
                    let jsUrl = out + file.replace('.mg', '.js')
                    new MagmaScript({
                        input: url,
                        output: jsUrl,
                        std: (file == this.std) ? true : false,
                        strict: this.strict
                    })
                }
            }
        }
    
        else {
            throw 'Specify input directory path and output directory path'
        }
    }
    
}


module.exports = MagmaScript