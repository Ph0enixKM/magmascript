#!/usr/bin/env node

const $root = ''
/**
 * Standard Library For MagmaScript Language
 * Copyright 2019 - Phoenix Arts (Paweł Karaś)
 */

function log(...content) {
    console.log(...content)
}

function random(min = 0, max = 1) {
    return (Math.random() * max) + min
}

function sqrt(number) {
    return Math.sqrt(number)
}

let get = () => log('Magma Warning: \'get\' function is not suppoerted in this enviroment');

// If involves Node.js compiler
if (typeof require == 'function') {
    get = (() => {
        const readline = require($root + 'readline-sync')

        function get(question = '', type, obj) {

            if (typeof type === 'undefined') {
                return readline.question(question)
            }

            if (type === 'password') {
                return readline.question(question, {
                    hideEchoBack: true
                })
            } else if (type === 'yes-no') {
                return readline.keyInYN(question)
            } else if (type === 'select') {
                return readline.keyInSelect(obj, question)
            } else if (type === 'loop') {
                if (question.length) console.log(question)
                return readline.promptCLLoop(obj)
            } else if (type === 'float') {
                return readline.questionFloat(question)
            } else if (type === 'int') {
                return readline.questionInt(question)
            } else if (type === 'email') {
                return readline.questionEMail(question)
            } else if (type === 'path') {
                return readline.questionPath(question)
            }
        }

        return get
    })()

}

Array.prototype.last = function() {
    return this[this.length - 1]
}

String.prototype.lower = function() {
    return this.toLowerCase()
}

String.prototype.upper = function() {
    return this.toUpperCase()
}

String.prototype.firstLower = function() {
    return this[0].toLowerCase() + this.substring(1)
}

String.prototype.firstUpper = function() {
    return this[0].toUpperCase() + this.substring(1)
}

// If involves HTML
if (typeof(document) != 'undefined') {
    function element(query) {
        return document.querySelector(query)
    }

    function elements(query) {
        return new Array(...document.querySelectorAll(query))
    }

    HTMLElement.prototype.css = function(key, val) {
        let newKey = ''

        for (let i = 0; i < key.length; i++) {
            if (key[i] == '-')
                newKey += key[++i].toUpperCase()
            else
                newKey += key[i]
        }

        this.style[newKey] = val
    }

    HTMLElement.prototype.magmaEvents = {}

    HTMLElement.prototype.on = function(event, callback, options = false) {
        let stateList = this.magmaEvents[event]

        this.magmaEvents[event] = (stateList == null) ? [] : stateList
        this.magmaEvents[event].push([callback, options])

        this.addEventListener(event, callback, options)

        return (this.magmaEvents[event].length - 1)
    }

    HTMLElement.prototype.detach = function(event, id = null) {
        if (isNaN(id)) {
            for (let index = 0; index < this.magmaEvents[event].length; index++) {
                this.removeEventListener(event, ...this.magmaEvents[event][index])
            }
        } else {
            this.removeEventListener(event, ...this.magmaEvents[event][id])
        }
    }

    HTMLElement.prototype.parent = function(up = 1) {
        let el = this

        for (; up > 0; up--) {
            el = el.parentElement
        }

        return el
    }
}

let Time = null
if (typeof require != 'undefined') {
    const {
        performance
    } = require('perf_hooks')

    class Time_Type {
        constructor() {
            this.timers = {}
            this.def = performance.now()
        }

        start(label) {
            if (label) {
                this.timers[label] = performance.now()
            } else {
                this.def = performance.now()
            }
        }

        stop(label) {
            let end = performance.now()

            if (label) {
                return end - this.timers[label]
            } else {
                return end - this.def
            }
        }

        now() {
            return new Date().getTime()
        }
    }

    Time = new Time_Type()
}

/**
 * STL Code:
 */
const File = (() => {
    const fs = require(`` + `fs`)

    class File {
        constructor(path) {
            this.path = path
        }

        read(encoding = `utf-8`) {
            return fs.readFileSync(this.path, encoding)
        }

    }

    return File
})()

/* Your Code Starts here: */

'use strict';

// @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:1
const path = require(`` + `path`)
// @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:2
const chalk = require(`` + `chalk`)
// @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:3
const MagmaScript = require(`` + path.join(__dirname, `../main.js`))
// @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:4

// @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:5
let args = process.argv
// @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:6

// @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:7

// @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:8
let help = chalk.yellow(`---MagmaCompiler---\n`) +
    // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:9
    `Usage: magma-script ` + chalk.cyan(`<file input> <file output>`) + `\n       mg ` + chalk.cyan(`<file input> <file output>`)
// @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:10

// @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:11

// @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:12
if (args[2] == `help`) {
    // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:13
    log(help)
    // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:14
    process.exit()
    // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:15
}
// @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:16

// @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:17
else {
    // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:18
    if (!args[2]) {
        // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:19
        log(chalk.red(`Specify input file. Run \'mg help\' for more information`))
        // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:20
        process.exit()
        // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:21
    }
    // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:22

    // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:23
    let out = args[3]
    // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:24
    let isSTD = args.includes(`--std`)
    // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:25

    // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:26
    if (out) {
        // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:27
        new MagmaScript({
            // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:28
            input: path.join(process.cwd(), args[2]),
            // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:29
            output: path.join(process.cwd(), out),
            // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:30
        })
        // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:31
    }
    // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:32

    // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:33

    // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:34
    else {
        // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:35
        eval(new MagmaScript({
                // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:36
                input: path.join(process.cwd(), args[2]),
                // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:37
                std: isSTD
                // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:38
            }).result
            // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:39
        )
        // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:40
    }
    // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:41

    // @magma /home/phoenix/Desktop/Projects/magmascript/bin/run.mg:42
}