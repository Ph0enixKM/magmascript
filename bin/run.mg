import path from 'path'
import chalk from 'chalk'
import MagmaScript from path.join(__dirname, '../main.js')

let args = process.argv

// Help message
let help = chalk.yellow('---MagmaCompiler---\n') + 
'Usage: magma-script ' + chalk.cyan('<file input> <file output>') + '
       mg ' + chalk.cyan('<file input> <file output>')


if(args[2] == 'help') {
    log(help)
    process.exit()
}

else {
    if (!args[2]) {
        log(chalk.red('Specify input file. Run \'mg help\' for more information'))
        process.exit()
    }

    let out = args[3]
    let isSTD = args.includes('--std')

    if (out) {
        new MagmaScript({
            input: path.join(process.cwd(), args[2]),
            output: path.join(process.cwd(), out),
        })
    }


    else {
        eval(
            new MagmaScript({
                input: path.join(process.cwd(), args[2]),
                std: isSTD
            }).result
        )
    }

}